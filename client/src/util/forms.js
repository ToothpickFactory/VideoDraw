export function submitContactForm(name, email, message) {
	const url = 'https://docs.google.com/forms/d/e/1FAIpQLSfrseWToLsVB5FODg1QxxfINx0ln3A8PhHV3aEAij3ChWjOyA/formResponse';

	let myHeaders = new Headers();
	myHeaders.append("Content-Type", "application/x-www-form-urlencoded");

	let urlencoded = new URLSearchParams();
	urlencoded.append("entry.2000002837", name);
	urlencoded.append("entry.1209048203", email);
	urlencoded.append("entry.686873740", message);

	const requestOptions = {
		method: 'POST',
		headers: myHeaders,
		body: urlencoded
	};

	fetch(url, requestOptions)
		.then(response => response.text())
		.then(result => console.log(result))
		.catch(error => console.log('error', error));
}
