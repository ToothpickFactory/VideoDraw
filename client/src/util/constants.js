export const colors = [
  {
    'name': "red",
    'code': '#FF0000'
  },
  {
    'name': "orange",
    'code': '#FF9600'
  },
  {
    'name': "yellow",
    'code': '#FFEA00'
  },
  {
    'name': "green",
    'code': '#66FF00'
  },
  {
    'name': "blue",
    'code': '#00F6FF'
  },
  {
    'name': "purple",
    'code': '#9C00FF'
  }
];
