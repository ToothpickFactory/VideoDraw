import { Component, h, Host, Method } from '@stencil/core';

@Component({
	tag: 'pop-box',
	styleUrl: 'pop-box.scss'
})
export class PopBox {
	private optionsEle: HTMLElement;

	@Method()
	async toggle() {
		this.optionsEle.classList.toggle('is-hidden');
	}

	render() {
		return (
			<Host>
				<button class="btn" onClick={() => this.toggle()}>
					<slot name="selection" />
				</button>
				<div class="pop-options is-hidden" ref={ele => this.optionsEle = ele}>
					<slot name="options" />
				</div>
			</Host>
		);
	}
}
