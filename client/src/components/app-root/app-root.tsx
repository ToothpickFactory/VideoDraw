import { Component, h, Host, State } from '@stencil/core';
import { VideoDraw } from '../../models/VideoDraw/VideoDraw';

enum AppState {
	SELECT_MEDIA,
	WORKSPACE
}

@Component({
	tag: 'app-root',
	styleUrl: 'app-root.scss'
})
export class AppRoot {
	@State()
	private appState: AppState = AppState.SELECT_MEDIA;

	private videoDraw: VideoDraw;

	private selectMedia(videoDraw: VideoDraw) {
		this.videoDraw = videoDraw;
		this.appState = AppState.WORKSPACE;
	}

	// private getBrowser = () => {
	// 	const isChrome: Boolean = !!(window as any).chrome && (!!(window as any).chrome.webstore || !!(window as any).chrome.runtime);
	// 	return isChrome;
	// }

	private resetAppState = () => {
		this.appState = AppState.SELECT_MEDIA;
	}

	render() {
		const viewStates = {
			[AppState.SELECT_MEDIA]: <media-select onMediaSelect={(videoDraw: VideoDraw) => this.selectMedia(videoDraw)} />,
			[AppState.WORKSPACE]: <work-space videoDraw={this.videoDraw} resetAppState={this.resetAppState} />
		}

		return (
			<Host>
				{viewStates[this.appState]}
				{/* {this.getBrowser() && viewStates[this.appState]} */}
				{/* {!this.getBrowser() && <div>Switch to Chrome, bozo</div>} */}
				<div id="copyright">2019 © Toothpick Factory LLC. All functions, designs, logos and functions are property of Toothpick Factory LLC.</div>
			</Host>
		);
	}
}
