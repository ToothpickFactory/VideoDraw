import { Component, h, Host, Prop, State } from '@stencil/core';
import { submitContactForm } from '../../util/forms.js'

@Component({
	tag: 'contact-us-modal',
	styleUrl: 'contact-us-modal.scss'
})
export class VideoControls {
  @State()
  private contactFormName: string;

  @State()
  private contactFormEmail: string;

  @State()
	private contactFormMessage: string;

	@Prop()
	public toggleModal

  handleChange(event) {
    this[event.target.name] = event.target.value;
  }

  private handleContactFormSubmit(e) {
    e.preventDefault();
		submitContactForm(this.contactFormName, this.contactFormEmail, this.contactFormMessage);
  }

	render() {
		return (
			<Host>
				<div class="modal" id="contact-us">
          <p>Please send us any comments, suggestions, <br /> or questions that you might have!</p>
          <form onSubmit={(e) => this.handleContactFormSubmit(e)}>
            <label>
              Name:
              <input type="text" required name="contactFormName" value={this.contactFormName} onInput={(e) => this.handleChange(e)} />
            </label>
            <label>
              Email:
              <input type="email" required name="contactFormEmail" value={this.contactFormEmail} onInput={(e) => this.handleChange(e)}/>
            </label>
            <label>
              Message:
              <textarea rows={10} required name="contactFormMessage" value={this.contactFormMessage} onInput={(e) => this.handleChange(e)}/>
            </label>
            <div>
              <button class='btn form-btn' type="submit">Submit</button>
              <button class='btn form-btn' type="reset" onClick={() => this.toggleModal("contact-us")}>Cancel</button>
            </div>
          </form>
        </div>
			</Host>
		);
	}
}
