import { Component, h, Host, Prop } from '@stencil/core';
import { VideoDraw } from '../../models/VideoDraw/VideoDraw';
import { Recorder } from '../../models/Recorder/Recorder';
import { Microphone } from '../../models/Microphone/Microphone';

@Component({
	tag: 'work-space',
	styleUrl: 'work-space.scss'
})
export class WorkSpace {
	@Prop()
	public videoDraw: VideoDraw;

	public recorder: Recorder = new Recorder();

	public microphone: Microphone = new Microphone();

	@Prop()
	public resetAppState: Function;

	componentWillLoad() {
		this.recorder.addStream(this.videoDraw.stream);
		this.microphone.addEventListener(Microphone.STREAM_AVAILABLE, () => {
			this.recorder.addStream(this.microphone.stream);
		});
	}

	render() {
		return (
			<Host>
				<side-bar videoDraw={this.videoDraw} recorder={this.recorder} microphone={this.microphone} resetAppState={this.resetAppState} />
				<div class="video-draw-container" ref={ref => ref.appendChild(this.videoDraw.canvas)}></div>
				<video-controls videoDraw={this.videoDraw} recorder={this.recorder} />
			</Host>
		);
	}
}
