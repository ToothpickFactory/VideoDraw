import { Component, h, Host, Prop, State } from '@stencil/core';
import { VideoDraw } from '../../models/VideoDraw/VideoDraw';
import { Recorder, RecorderState } from '../../models/Recorder/Recorder';
import { Microphone, MicrophoneState } from '../../models/Microphone/Microphone';
import { colors } from '../../util/constants.js'

@Component({
	tag: 'side-bar',
	styleUrl: 'side-bar.scss'
})
export class VideoControls {
	@Prop()
	public videoDraw: VideoDraw;

	@Prop()
	public recorder: Recorder;

	@Prop()
	public microphone: Microphone;

	@Prop()
	public resetAppState: Function;

	@State()
	private microphoneState: MicrophoneState = this.microphone.state;

	@State()
	private recordState: string = this.recorder.getRecorderState();

	private selectedColorEle: HTMLElement;

	public componentDidLoad() {
		this.microphone.addEventListener(Microphone.STATE_CHANGE, this.handleMicStateChange);
		this.recorder.addEventListener(Recorder.STATE_CHANGE, this.handleRecorderStateChange);
		this.selectedColorEle.style.backgroundColor = this.videoDraw.getStrokeColor();
	}

	public componentDidUnload() {
		this.microphone.removeEventListener(Microphone.STATE_CHANGE, this.handleMicStateChange);
		this.recorder.removeEventListener(Recorder.STATE_CHANGE, this.handleRecorderStateChange);
	}

	private handleMicStateChange = (e: CustomEvent) => {
		this.microphoneState = e.detail;
	}

	private handleRecorderStateChange = (e: CustomEvent) => {
		this.recordState = e.detail;
	}

	private setStrokeColor = (e) => {
		this.videoDraw.setStrokeColor(e.srcElement.style.backgroundColor);
		this.selectedColorEle.style.backgroundColor = this.videoDraw.getStrokeColor();
		e.srcElement.closest('pop-box').toggle();
	}

	public toggleModal(id) {
		document.getElementById(id).classList.toggle('show');
	}

	private handleSubmit(e) {
		e.preventDefault()
		this.recorder.setFileName(e.target[0].value);
		this.recorder.download();
		document.getElementById('name-file-form').classList.toggle('show');
	}

	handleChange(event) {
		this[event.target.name] = event.target.value;
	}

	render() {
		const microphoneButton = {
			[Microphone.State.NO_INIT]: <button class="btn" onClick={() => this.microphone.toggle()}> <i class="icon-mic-off" /></button>,
			[Microphone.State.INACTIVE]: <button class="btn" onClick={() => this.microphone.toggle()}> <i class="icon-mic-off" /></button>,
			[Microphone.State.ACTIVE]: <button class="btn" onClick={() => this.microphone.toggle()}> <i class="icon-mic" /></button>
		}

		const recordingButton = {
			[RecorderState.INACTIVE]: <button class="btn record" onClick={() => this.recorder.start()}>REC </button>,
			[RecorderState.PAUSED]: <button class="btn record" onClick={() => this.recorder.start()}>REC</button>,
			[RecorderState.RECORDING]: <button class="btn record active" onClick={() => this.recorder.pause()}>REC</button>,
		}

		return (
			<Host>
				<div class="dropdown">
					<button class="btn menu-button"><img src="assets/icons/logo.png" alt="VideoDraw Logo" class="logo-icon" /></button>
					<div class="dropdown-content">
						<a href="#" onClick={() => this.resetAppState()}>New File</a>
						<a href="#" onClick={() => this.toggleModal("contact-us")}>Contact Us</a>
						{/* <a href="#">Subscribe for Updates</a> */}
					</div>
				</div>

				<pop-box class="color-picker">
					<span slot="selection" class="color-box" ref={ele => this.selectedColorEle = ele}></span>
					<span slot="options" class="color-options">{colors.map(color => <span class='color-box' style={{ backgroundColor: color.code }} onClick={(e) => this.setStrokeColor(e)}></span>)}</span>
				</pop-box>

				<pop-box class="stroke-width-select">
					<i slot="selection" class="icon-line_weight" />
					<input slot="options" type="range" class="slider" min="1" max="100" step="1" value={this.videoDraw.getLineWidth()} onInput={(e: any) => this.videoDraw.setLineWidth(e.target.value)} />
				</pop-box>

				<button class="btn" onClick={() => this.videoDraw.clearDrawings()}>CLR</button>

				{/* Spacer */}
				<span></span>

				{microphoneButton[this.microphoneState]}

				<button class="btn" onClick={() => this.toggleModal('name-file-form')}><i class="icon-download" /></button>

				{recordingButton[this.recordState]}

				<div class="modal" id="name-file-form">
					<form onSubmit={(e) => this.handleSubmit(e)}>
						<label>
							File&nbsp;Name:
							<input type="text" value="VideoDraw.io" />
						</label>
						<button class='btn submit-form' type="submit"><i class="icon-download" /></button>
					</form>
				</div>

				<contact-us-modal toggleModal={this.toggleModal} />
			</Host>
		);
	}
}
