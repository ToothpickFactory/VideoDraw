import { Component, h, Host, Prop } from '@stencil/core';
import { VideoDraw } from './../../models/VideoDraw/VideoDraw';

@Component({
	tag: 'media-select',
	styleUrl: 'media-select.scss'
})
export class MediaSelect {
	@Prop()
	public onMediaSelect: (videoDraw: VideoDraw) => void;

	private async selectMedia(input: HTMLInputElement) {
		const file = input.files[0];
		const videoUrl = URL.createObjectURL(file);
		const videoDraw = await VideoDraw.setup(videoUrl);
		this.onMediaSelect(videoDraw);
	}

	render() {
		return (
			<Host>
				<span class="logo-icon-box"><img src="assets/icons/logo.png" alt="VideoDraw Logo" class="logo-icon" /> video<span class="bold">draw</span></span>
				<label>
					<input type="file" onChange={(e: Event) => this.selectMedia(e.target as HTMLInputElement)} />
					<i class="icon-upload upload-icon" />
				</label>
				<p>drag and drop a <br /> video file <br /> - or - <br /> click to select a file</p>
			</Host>
		);
	}
}
<i class="icon-play" />
