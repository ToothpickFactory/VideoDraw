import { Component, h, Host, Prop, State } from '@stencil/core';
import { VideoDraw, PlayerState } from '../../models/VideoDraw/VideoDraw';
import { Recorder } from '../../models/Recorder/Recorder';

@Component({
	tag: 'video-controls',
	styleUrl: 'video-controls.scss'
})
export class VideoControls {
	@Prop()
	public videoDraw: VideoDraw;

	@Prop()
	public recorder: Recorder;

	@State()
	private videoState: string = PlayerState.PAUSED;

	@State()
	private playerTime: number = 0;

	private rangeEle?: any;

	public componentDidLoad() {
		/**
		 * Video Player Controls
		 */
		const videoEle = this.videoDraw.videoEle;

		this.rangeEle.max = videoEle.duration;

		videoEle.addEventListener('timeupdate', () => {
			this.playerTime = videoEle.currentTime;
		});

		videoEle.addEventListener('play', () => {
			this.videoState = PlayerState.PLAYING;
		});

		videoEle.addEventListener('pause', () => {
			this.videoState = PlayerState.PAUSED;
		});

		this.rangeEle.oninput = () => {
			videoEle.currentTime = this.rangeEle.value;
		}
	}

	private handleSpeedSelect = (e) => {
		const target = e.target as HTMLSelectElement;
		this.videoDraw.videoEle.playbackRate = Number(target.value);
	}

	private setVolume(event) {
		this.videoDraw.videoEle.volume = event.target.value / 100;
	}

	private closeModal() {
		document.getElementById('warning-modal').classList.remove('show');
	}

	private playVideo() {
		if (this.recorder.getRecorderState() !== 'recording') {
			document.getElementById('warning-modal').classList.add('show');
		}
		this.videoDraw.videoEle.play()
	}

	private timeFormat(time: number) {
		const seconds = ~~time % 60;
		const minutes = ~~(time / 60) % 60;
		return minutes + ":" + (seconds < 10 ? '0' + seconds : seconds);
	}

	render() {
		const playerButton = {
			[PlayerState.PLAYING]: <button class='btn' onClick={() => this.videoDraw.videoEle.pause()}><i class="icon-pause" /></button>,
			[PlayerState.PAUSED]: <button class='btn' onClick={() => this.playVideo()}><i class="icon-play_arrow" /></button>
		}

		return (
			<Host>

				{playerButton[this.videoState]}

				<pop-box class="color-picker">
					<i slot="selection" class="icon-mute" />
					<input slot="options" class="slider" type="range" min="0" max="100" step="1" onInput={(event) => this.setVolume(event)} />
				</pop-box>

				<select class="btn speed-select" onInput={(event) => this.handleSpeedSelect(event)}>
					<option value={2.0}>2x</option>
					<option value={1.0} selected>1x</option>
					<option value={.5}>.5x</option>
				</select>

				<div class="scrub-container">
					<span>{this.timeFormat(this.videoDraw.videoEle.currentTime)}</span>
					<input type="range" class="slider" step="1" ref={el => this.rangeEle = el} value={this.playerTime} />
					<span>{this.timeFormat(this.videoDraw.videoEle.duration)}</span>
				</div>

				<div class="warning-modal" id="warning-modal">
					<button class="close-modal" onClick={() => this.closeModal()}>X</button>
					<div class="recording-warning">
						<h1>- WARNING -</h1>
						<p>You are currently <br /> not recording.</p>
						<p>Please press the record button <br /> to begin recording your edits.</p>
					</div>
				</div>

			</Host>
		);
	}
}
