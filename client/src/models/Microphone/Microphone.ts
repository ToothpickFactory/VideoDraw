export enum MicrophoneState {
	NO_INIT = -1,
	DENIED = 0,
	ALLOWED = 1,
	INACTIVE = 2,
	ACTIVE = 3
}

export class Microphone extends EventTarget {
	public static State = MicrophoneState;
	public static STATE_CHANGE = 'STATE_CHANGE';
	public static STREAM_AVAILABLE = 'STREAM_AVAILABLE';

	public stream: MediaStream;
	public state: MicrophoneState = Microphone.State.NO_INIT;

	constructor() {
		super();
	}

	public async toggle() {
		switch (this.state) {
			case Microphone.State.NO_INIT:
				return this.enable();
			case Microphone.State.DENIED:
				return Microphone.State.DENIED;
			case Microphone.State.ACTIVE:
				return this.mute();
			case Microphone.State.INACTIVE:
				return this.unmute();
		}
	}

	public async enable() {
		try {
			this.stream = await navigator.mediaDevices.getUserMedia({ audio: true, video: false });
			this.dispatchEvent(new CustomEvent(Microphone.STREAM_AVAILABLE))
			this.setState(Microphone.State.ACTIVE);
		} catch (err) {
			this.setState(Microphone.State.DENIED);
		}
	}

	public mute() {
		this.stream.getAudioTracks().forEach((track: MediaStreamTrack) => track.enabled = false);
		this.setState(Microphone.State.INACTIVE);
	}

	public unmute() {
		this.stream.getAudioTracks().forEach((track: MediaStreamTrack) => track.enabled = true);
		this.setState(Microphone.State.ACTIVE);
	}

	private setState(state: MicrophoneState) {
		this.state = state;
		this.dispatchEvent(new CustomEvent(Microphone.STATE_CHANGE, { detail: this.state }));
	}
}
