interface HTMLCanvasElementPlus extends HTMLCanvasElement {
	captureStream?: (fps?: number) => MediaStream;
}

interface HTMLVideoElementPlus extends HTMLVideoElement {
	captureStream?: () => MediaStream;
}

interface Coordinate {
	x: number;
	y: number;
}

interface Stroke {
	createdAt: number,
	endedAt: number | null,
	strokeStyle: string;
	lineWidth: number,
	lineCap: CanvasLineCap,
	coords: Coordinate[]
};

export enum PlayerState {
	PLAYING = 'PLAYING',
	PAUSED = 'PAUSED'
}

export class VideoDraw {
	public static async setup(videoUrl: string): Promise<VideoDraw> {
		const videoDraw = new VideoDraw(videoUrl);
		await videoDraw.init();
		return videoDraw;
	}

	private drawing: boolean = false;
	private strokes: Stroke[] = [];
	private currentStroke: Stroke;
	private strokeColor: string = '#FFEA00';
	private lineWidth: number = 10;
	private canvasStream: MediaStream;

	public videoEle: HTMLVideoElementPlus = document.createElement("video");
	public canvas: HTMLCanvasElementPlus = document.createElement("canvas");
	public ctx: CanvasRenderingContext2D = this.canvas.getContext('2d');
	public stream: MediaStream;

	constructor(public videoUrl: string) {
		this.canvas.addEventListener("mousemove", this.handlePenMove);
		this.canvas.addEventListener("mousedown", this.handlePenDown);
		this.canvas.addEventListener("mouseup", this.handlePenUp);
		this.canvas.addEventListener("mouseout", this.handlePenUp);
		this.canvas.addEventListener("touchmove", this.handlePenMove, { passive: true });
		this.canvas.addEventListener("touchstart", this.handlePenDown, { passive: true });
		this.canvas.addEventListener("touchend", this.handlePenUp, { passive: true });
		this.canvasStream = this.canvas.captureStream();
	}

	public init = async () => {
		await new Promise((resolve, _reject) => {
			this.videoEle.setAttribute("src", `${this.videoUrl}#t=0.1`);
			this.videoEle.addEventListener("loadedmetadata", resolve);
		});

		const videoEleStream = this.videoEle.captureStream();
		const ctx = new AudioContext();
		const dest = ctx.createMediaStreamDestination();
		ctx.createMediaStreamSource(videoEleStream).connect(dest);
		const videoTrack = this.canvasStream.getTracks()[0];
		const audioTrack = dest.stream.getTracks()[0];
		this.stream = new MediaStream([videoTrack, audioTrack]);

		this.canvas.height = this.videoEle.videoHeight;
		this.canvas.width = this.videoEle.videoWidth;
		this.capture();
	}

	public getStrokeColor = (): string => {
		return this.strokeColor;
	}

	public setStrokeColor = (color: string): void => {
		this.strokeColor = color;
	}

	public getLineWidth = (): number => {
		return this.lineWidth;
	}

	public setLineWidth = (lineWidth: number): void => {
		this.lineWidth = lineWidth;
	}

	public clearDrawings = (): void => {
		this.strokes = [];
	}

	private capture = () => {
		this.ctx.globalAlpha = 1;
		this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
		this.ctx.drawImage(this.videoEle, 0, 0);
		this.strokes.forEach(this.drawStroke)
		requestAnimationFrame(this.capture);
	}

	private drawStroke = (stroke: Stroke) => {
		if (!stroke.coords.length) return;
		const firstCoord = stroke.coords[0];
		this.ctx.strokeStyle = stroke.strokeStyle;
		this.ctx.lineWidth = stroke.lineWidth;
		this.ctx.lineCap = stroke.lineCap;

		this.ctx.beginPath();
		this.ctx.moveTo(firstCoord.x, firstCoord.y);
		for (let i = 1; i < stroke.coords.length; i++) {
			this.ctx.lineTo(stroke.coords[i].x, stroke.coords[i].y);
		}
		this.ctx.stroke();
	}

	private handlePenMove = (e: any) => {
		if (!this.drawing) return;
		const rect = this.canvas.getBoundingClientRect();
		const scaleX = this.canvas.width / rect.width;
		const scaleY = this.canvas.height / rect.height;
		let target = e.targetTouches && e.targetTouches[0] || e;
		const pos = {
			x: (target.clientX - rect.left) * scaleX,
			y: (target.clientY - rect.top) * scaleY
		}
		this.currentStroke.coords.push(pos);
	}

	private handlePenDown = () => {
		if (!this.drawing) {
			this.drawing = true;
			this.currentStroke = {
				createdAt: performance.now(),
				endedAt: null,
				strokeStyle: this.strokeColor,
				lineWidth: this.lineWidth,
				lineCap: "round",
				coords: []
			};
			this.strokes.push(this.currentStroke);
		}
	}

	private handlePenUp = () => {
		if (this.drawing) {
			this.drawing = false;
			this.currentStroke.endedAt = performance.now();
		}
	}
}

// Fade logic
// this.ctx.globalAlpha = 1;
// if (stroke.endedAt) {
// 	const fadeTime = 3000;
// 	const elapsedTime = performance.now() - stroke.endedAt;
// 	if (elapsedTime > fadeTime) return;
// 	const opacity = 1 - elapsedTime / fadeTime;
// 	this.ctx.globalAlpha = opacity > 0 ? opacity : 0;
// }
