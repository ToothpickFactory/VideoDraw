export enum RecorderState {
	INACTIVE = 'inactive',
	RECORDING = 'recording',
	PAUSED = 'paused'
}

export class Recorder extends EventTarget {
	public static STATE_CHANGE = 'STATE_CHANGE';

	public mediaStream: MediaStream = new MediaStream();
	public mediaRecorder: MediaRecorder = new MediaRecorder(this.mediaStream, { mimeType: 'video/webm;codecs=H264' });
	private recordedChunks: Blob[] = [];
	private fileName: string = 'VideoDraw.io';

	constructor() {
		super();
		this.mediaRecorder.ondataavailable = this.handleDataAvailable;
		this.mediaRecorder.addEventListener('start', () => this.emitState());
		this.mediaRecorder.addEventListener('pause', () => this.emitState());
		this.mediaRecorder.addEventListener('resume', () => this.emitState());
		this.mediaRecorder.addEventListener('stop', () => this.emitState());
	}

	public getRecorderState() {
		return this.mediaRecorder.state;
	}

	public start() {
		this.mediaRecorder.state === RecorderState.PAUSED
			? this.mediaRecorder.resume()
			: this.mediaRecorder.start();
	}

	public pause() {
		this.mediaRecorder.pause();
	}

	public async stop() {
		return new Promise((resolve) => {
			this.mediaRecorder.onstop = resolve;
			this.mediaRecorder.stop();
		});
	}

	public resume() {
		this.mediaRecorder.resume();
	}

	public addStream(stream: MediaStream) {
		// Can not add streams while recording
		if (this.mediaRecorder.state !== RecorderState.INACTIVE)
			throw new Error('CAN NOT ADD TRACK WHILE RECORDING');

		// Video Track
		const videoTrack = stream.getVideoTracks()[0];
		if (videoTrack) this.mediaStream.addTrack(stream.getVideoTracks()[0]);

		// Add Audio Tracks
		const audioTrack = stream.getAudioTracks()[0];
		const recorderAudioTrack = this.mediaStream.getAudioTracks()[0];

		if (audioTrack) {
			const ctx = new AudioContext();
			const dest = ctx.createMediaStreamDestination();
			ctx.createMediaStreamSource(stream).connect(dest);
			if (recorderAudioTrack) {
				ctx.createMediaStreamSource(this.mediaStream).connect(dest);
				this.mediaStream.removeTrack(recorderAudioTrack);
			}
			const mergedAudioTrack = dest.stream.getTracks()[0];
			this.mediaStream.addTrack(mergedAudioTrack)
		}
	}

	public setFileName(name) {
		this.fileName = name;
	}

	public async download() {
		if (this.mediaRecorder.state !== RecorderState.INACTIVE) await this.stop();
		const blob = new Blob(this.recordedChunks, { type: 'video/webm;codecs=H264' });
		const url = URL.createObjectURL(blob);
		const a = document.createElement('a') as any;
		document.body.appendChild(a);
		a.style = 'display: none';
		a.href = url;
		a.download = `${this.fileName}.webm`;
		a.click();
		window.URL.revokeObjectURL(url);
	}

	private handleDataAvailable = (event: BlobEvent) => {
		if (event.data.size > 0) {
			this.recordedChunks.push(event.data);
		}
	}

	private emitState = () => {
		this.dispatchEvent(new CustomEvent(Recorder.STATE_CHANGE, { detail: this.mediaRecorder.state }))
	}
}
